<?php

use yii\db\Migration;

/**
 * Class m240426_134653_init_rbac
 */
class m240426_134653_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $viewUsers = $auth->createPermission('viewUsers');
        $viewUsers->description = 'View the users';
        $auth->add($viewUsers);

        // добавляем разрешение "manageUsers"
        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Manage users';
        $auth->add($manageUsers);


        // add "engineer" role and give this role the "viewUsers" permission
        $engineer = $auth->createRole('engineer');
        $auth->add($engineer);
        $auth->addChild($engineer, $viewUsers);


        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $engineer);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($engineer, 3);
        $auth->assign($admin, 2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        echo "m240426_134653_init_rbac cannot be reverted.\n";
//
//        return false;
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240426_134653_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
