<?php

use yii\db\Migration;

/**
 * Class m240426_081044_add_user_data
 */
class m240426_081044_add_user_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 2,
            'username' => 'admin',
            'auth_key' => '',
            'password_hash' => '$2y$13$pCGOMcahIXhMgM9vE2D2N.h52gkkmxsUhHuOApsE2TOtlf9kzhsd2',
            'password_reset_token' => null,
            'email' => 'admin@example.com',

            'status' => 10,
            'created_at' => (new DateTime())->getTimestamp(),
            'updated_at' => (new DateTime())->getTimestamp(),
            'is_admin' => true,
            'is_working' => true,
        ]);
        $this->insert('{{%user}}', [
            'id' => 3,
            'username' => 'user',
            'auth_key' => '',
            'password_hash' => '$2y$13$pCGOMcahIXhMgM9vE2D2N.h52gkkmxsUhHuOApsE2TOtlf9kzhsd2',
            'password_reset_token' => null,
            'email' => 'user@example.com',
            'status' => 10,
            'created_at' => (new DateTime())->getTimestamp(),
            'updated_at' => (new DateTime())->getTimestamp(),
            'is_admin' => false,
            'is_working' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240426_081044_add_user_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240426_081044_add_user_data cannot be reverted.\n";

        return false;
    }
    */
}
