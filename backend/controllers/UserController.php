<?php

namespace backend\controllers;

use app\models\AuthAssignment;
use common\models\User;
use common\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['index', 'view'],
                            'roles' => ['viewUsers'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['create', 'update', 'delete'],
                            'roles' => ['manageUsers'],
                        ]
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     *
     * @return string|Response
     */
    public function actionIndex()
    {


            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search($this->request->queryParams);

            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);


    }

    /**
     * Displays a single User model.
     * @param int $id
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $role = AuthAssignment::findOne([
            'user_id' => $id
        ]);

        return $this->render('view', [
                'model' => $this->findModel($id),
                'role' => $role,
            ]);



    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|Response
     */
    public function actionCreate()
    {

            $model = new User();
            $role = new AuthAssignment();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {

                    // нужно добавить следующие три строки:
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole($this->request->post()['AuthAssignment']['item_name']);
                    $auth->assign($authorRole, $model->getId());
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                'model' => $model,
                'role' => $role,
            ]);


    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id
     * @return string|Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

            $model = $this->findModel($id);
            $role = AuthAssignment::findOne([
                'user_id' => $model->id
            ]);

            if (!$role){
                $auth = Yii::$app->authManager;
                $authorRole = $auth->getRole('engineer');
                $auth->assign($authorRole, $model->getId());

            }
            $role = AuthAssignment::findOne([
                'user_id' => $model->id
            ]);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                $role->item_name = $this->request->post()['AuthAssignment']['item_name'];
                $role->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
                'role' => $role,
            ]);

    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {


            $this->findModel($id)->delete();

            return $this->redirect(['index']);

    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
