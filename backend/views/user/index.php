<?php

use common\models\User;
use yii\bootstrap5\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var common\models\UserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php

    ?>


    <?php
    Pjax::begin();
    // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            'status',
            'created_at:datetime',
            'updated_at:datetime',
            //'verification_token',
            'is_working',
            [
                'class' => ActionColumn::className(),
                'options'=>['class'=>'action-column'],
                'template' => '{view} {update} {delete} {modal}',
                'buttons' => [
                  'modal' => function ($url, $model, $key) {
                      $btn = Html::button("
                      <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pencil' viewBox='0 0 16 16''>
                      <path d='M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325'></path>
                    </svg>",[
                          'value'=>Yii::$app->urlManager->createUrl('user/update?id='.$model->id), //<---- here is where you define the action that handles the ajax request
                          'class'=>'update-modal-click grid-action btn btn-outline-secondary',
                          'data-toggle'=>'tooltip',
                          'data-placement'=>'bottom',
                          'title'=>'Update'
                      ]);
                      return $btn;
                  },
                    'update' => function ($url, $model, $key) {
                        return '';
                    }
                ],
                'urlCreator' => function ($action, User $model, $key, $index, $column) {

                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],

        ],
    ]);

Pjax::end();

    ?>


</div>

<?php $this->registerJs(
    "$(function () {
        $('.update-modal-click').click(function () {
            $('#update-modal')
            .modal('show')
            .find('#updateModalContent')
            .load($(this).attr('value'));
        });
    });
    "
); ?>

<?php
Modal::begin([
    'title'=>'<h4>Update Model</h4>',
    'id'=>'update-modal',
    'size'=>'modal-lg'
]);

echo "<div id='updateModalContent'></div>";

Modal::end();
?>
